﻿using System;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using App;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TFGUtils
{
    public static class JsonHelper
    {

        public static AppSettings LoadSettings(string dataPath, string fileName)
        {
            //Application dataPath is the AppSettings folder of the executable
            string finalPath = Path.Combine(dataPath, fileName);
            AppSettings settings = LoadJson(finalPath);
            settings?.SanitizeData();
            return settings;
        }
        
        public static void SaveDefaultSettings(AppSettings settings, string dataPath, string fileName)
        {
            settings.SanitizeData();
            dataPath = Path.Combine(dataPath, fileName);
            SaveJson(settings, dataPath);
        }
        
        public static void SaveStartStopResults(StartStopTrialData measurings, string dataPath)
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            string fileName = DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss");
            // if needed sanitation or casting, it would be here
            string finalFileName = fileName + " - " + currentSceneName + ".json";
            MonoBehaviour.print(finalFileName);
            dataPath = Path.Combine(dataPath, finalFileName);
            SaveJson(measurings, dataPath);
        }
        
        public static void SaveTrackDrivingResults(StartStopTrialData measurings, string dataPath)
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            string fileName = DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss");
            // if needed sanitation or casting, it would be here
            string finalFileName = fileName + " - " + currentSceneName + ".json";
            MonoBehaviour.print(finalFileName);
            dataPath = Path.Combine(dataPath, finalFileName);
            SaveJson(measurings, dataPath);
        }

        private static void SaveJson <T> (T data, string path)
        {
            string jsonString = JsonUtility.ToJson(data, true);

            using (StreamWriter streamWriter = File.CreateText(path))
            {
                streamWriter.Write(jsonString);
            }
        }

        private static AppSettings LoadJson(string path)
        {
            try
            {
                using (StreamReader streamReader = File.OpenText(path))
                {
                    string jsonString = streamReader.ReadToEnd();
                    return JsonUtility.FromJson<AppSettings>(jsonString);
                }
            }
            catch
            {
                MonoBehaviour.print("Fichero de configuración de la prueba: \"" + path + "\" no encontrado. Se procedera a crear uno con valores por defecto.");
                return null;
            }
        }
    }
}