﻿using RoadManagement;
using UnityEngine;
using TFGUtils;
using UnityEngine.SceneManagement;

namespace App
{
    public class AppControl : MonoBehaviour
    {
        // Singleton instance
        public static AppControl Global;
    
        // App Global SavedSettings
        public bool Debuging = false;
        public TrafficLightController[] TrafficArray;
        
        public AppSettings SavedSettings;
        public const int DefaultNumberOfSegments = 4;
        
        // Class to read/write to JSON
        //// vars with the name and path to the settings files
        public string DataPath;
        private string fileName = "AppSettings.json";
    

        void Awake ()   
        {
            if (Global == null)
            {
                DontDestroyOnLoad(gameObject);
                Global = this;
                DataPath = Application.dataPath;
                LoadAppSettings();
            }
            else if (Global != this)
            {
                Destroy (gameObject);
                return;
            }
        }

        void LoadAppSettings()
        {
            SavedSettings = JsonHelper.LoadSettings(DataPath, fileName);
            if (SavedSettings == null)
            {
                SavedSettings = new AppSettings(DefaultNumberOfSegments);
                JsonHelper.SaveDefaultSettings(SavedSettings, DataPath, fileName);
            }
        
        }

        private void Update()
        {

            if (Input.GetKeyDown(KeyCode.F10))
            {
                JsonHelper.SaveDefaultSettings(SavedSettings, DataPath, fileName);
            }

            if (Input.GetKeyDown(KeyCode.F9))
            {
                SavedSettings = JsonHelper.LoadSettings(DataPath, fileName);
            }
        }

        public void ChangeScene(int sceneBuildIndex)
        {
            SceneManager.LoadScene(sceneBuildIndex);

        }

        public void ExitApp()
        {
            Application.Quit();
#if (UNITY_EDITOR)
            Debug.Log("Game is now exiting");
#endif
        }

    }
}
