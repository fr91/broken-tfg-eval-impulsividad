﻿using System;

namespace App
{
    [Serializable]
    public class TrafficLightData
    {
        public bool HasAmber = false;
        public string SegmentLength = "S";
        public float ToGreenTimer = 2f;
        public float ToRedTimer = 3f;
    }
}
