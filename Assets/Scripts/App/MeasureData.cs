using System;
using System.Collections.Generic;

namespace App
{
    [Serializable]
    public class MeasureData
    {
        //Measurings:
        public bool StartedOnGreen;
        public float ElapsedSinceGreen;    // Seconds
        public bool StopOverflow;
        public float MetersToStopLine;    // Meters
        public int CommissionTimes = 0;
        public List<float> CommissionSpots;    // Meters (positions in the road)
    }
}