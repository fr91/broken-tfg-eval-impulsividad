using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Driving
{
//
// Summary:
//     Data structure that contains suspension info.
    [System.Serializable]
    public class SuspensionData
    {
        public bool HitCheck;
        public RaycastHit HitAttr;
        private float stiffness;
        public float SuspensionLength;
        public float CompressionRatio
        {
            get
            {
                return HitCheck ? (SuspensionLength - HitAttr.distance) / SuspensionLength : 0.0f;
            }
        }
        public Transform VehicleTransform;
        private Vector3 localSuspensionPoint;
        public Vector3 SuspensionPoint
        {
            get { return VehicleTransform.TransformPoint(localSuspensionPoint); }
            set { localSuspensionPoint = value; }
        }

        public SuspensionData(Transform vehicleTransform, Vector3 wheelLocalPosition, float suspensionLength)
        {
            HitCheck = false;
            VehicleTransform = vehicleTransform;
            localSuspensionPoint = wheelLocalPosition;
            SuspensionLength = suspensionLength;
            stiffness = 5.0f;
        }

        public SuspensionData(Transform vehicleTransform, Vector3 wheelLocalPosition, float suspensionLength, float suspensionStiffness)
        {
            HitCheck = false;
            VehicleTransform = vehicleTransform;
            localSuspensionPoint = wheelLocalPosition;
            SuspensionLength = suspensionLength;
            stiffness = suspensionStiffness;
        }

        public void ChangeSuspensionSettings(float newLength, float newStiffness)
        {
            SuspensionLength = newLength;
            stiffness = newStiffness;
        }
    }


//
// Summary:
//     Simulates multiple wheel arcade-like suspension of a vehicle through raycasts & forces.
    [RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
    public class CarSuspensionSystem : MonoBehaviour
    {
        private Transform myTransform;    // vehicle's transform NOT wheel's transform 
        private BoxCollider carcollider;
        private Rigidbody rigidcar;

        public bool DebugSuspension = false;

        // Wheels: GameObjects / characteristics / Velocity of the suspension response
        public GameObject[] WheelObjects;
        internal float[] WheelRadius;
        public float RadiusOffset = 0.06f;
        private Vector3[] modelDampingVel;

        // Suspension
        private SuspensionData[] wheelData;
        public float Stiffness = 5.0f;
        public float SuspensionLength = 0.6f;
        internal Vector3 AvgSurfaceNormal = new Vector3(0.0f, 1.0f, 0.0f);  // Initialize to normal vector from default plane (Vector.up)
        internal bool OffTheGround = false;
        public int NumberOfRayHits;

        // Awake is called when the script instance is being loaded
        private void Awake()
        {
            myTransform = GetComponent<Transform>();
            carcollider = GetComponent<BoxCollider>();
            rigidcar = GetComponent<Rigidbody>();
            //rigidcar.centerOfMass = new Vector3(rigidcar.centerOfMass.x, rigidcar.centerOfMass.y, (WheelObjects[0].transform.localPosition.z + WheelObjects[3].transform.localPosition.z) * 0.495f);

            InitializeSuspensionPoints();
        }

        // Start is called just before any of the Update methods is called the first time
        private void Start()
        {
        }

        // Update is called every frame, if the MonoBehaviour is enabled
        private void Update()
        {
            for (int i = 0; i< WheelObjects.Length; i++)
            {
                CastSuspensionRay(wheelData[i]);
                ModelSuspension(wheelData[i], WheelObjects[i], WheelRadius[i], modelDampingVel[i]);
            }
#if (UNITY_EDITOR)
            if (DebugSuspension)
            {
                DrawDebugRays();
            }
#endif
        }


        // This function is called every fixed framerate frame, if the MonoBehaviour is enabled
        private void FixedUpdate()
        {
            ResetFlippedCar();
            UpdateSuspensionSystem();
        }

#if (UNITY_EDITOR)
        // OnGUI is called for rendering and handling GUI events
        private void OnGUI()
        {
            if (DebugSuspension)
            {
                DrawDebugLabels();
            }
        }
#endif
        // This function is called when the script is loaded or a value is changed in the Inspector (Called in the editor only).
        private void OnValidate()
        {
            if (wheelData == null || wheelData.Length <= 0) return;        // If not initialized (outside play mode), then return
            if (Math.Abs(SuspensionLength) < 0.0001)                       // If new SuspensionLength is 0.0f, it would try to divide by zero
            {
                SuspensionLength = wheelData[0].SuspensionLength;          // Write back the last valid suspension in the editor
                return;
            }
            for (int i = 0; i < WheelObjects.Length; i++)
            {
                wheelData[i].ChangeSuspensionSettings(SuspensionLength, Stiffness);
            }
        }

        private void InitializeSuspensionPoints()
        {
            wheelData = new SuspensionData[WheelObjects.Length];
            WheelRadius = new float[WheelObjects.Length];
            modelDampingVel = new Vector3[WheelObjects.Length];
            for (int i = 0; i < WheelObjects.Length; i++)
            {
                // Initialize the origin of the suspension raycast to the local position of the wheels models
                MeshFilter wheelMesh = WheelObjects[i].GetComponent<MeshFilter>();
                WheelRadius[i] = wheelMesh.sharedMesh.bounds.extents.y + RadiusOffset;
                //print("wheel radius[" + i + "]: " +  WheelRadius[i]);
                //suspensionPoint = new Vector3(WheelObjects[i].transform.localPosition.x, WheelObjects[i].transform.localPosition.y - WheelRadius[i] * 0.25f, WheelObjects[i].transform.localPosition.z);
                Vector3 suspensionPoint = WheelObjects[i].transform.localPosition;
                suspensionPoint.y = carcollider.center.y - carcollider.bounds.extents.y + 0.05f;
                wheelData[i] = new SuspensionData(myTransform, suspensionPoint, SuspensionLength, Stiffness);
            }
            return;
        }

        private bool CastSuspensionRay(SuspensionData wData)
        {
            int trafficLightsLayer = (1 << 8);// | (1 << 10);
            trafficLightsLayer = ~trafficLightsLayer;
            Vector3 up = myTransform.up;
            wData.HitCheck = Physics.Raycast(wData.SuspensionPoint, -up, out wData.HitAttr, wData.SuspensionLength, trafficLightsLayer);
            if (wData.HitCheck == false)
            {
                // if there was no hit, we have to touch the values inside the struct to represent the max length of the suspension
                wData.HitAttr.normal = myTransform.up;
                wData.HitAttr.point = wData.SuspensionPoint - up * wData.SuspensionLength;
                wData.HitAttr.distance = wData.SuspensionLength;
            }
            return wData.HitCheck;
        }

        private float ComputeSuspension(SuspensionData wData, ForceMode mode)
        {
            if (wData.HitCheck)
            {
                Vector3 up = myTransform.up;
                Vector3 currentSusp, newSusp, deltaSusp;
                // CompressionRatio = (suspensionLength - HitAttr.distance) / suspensionLength;
                currentSusp = Vector3.Project(rigidcar.GetPointVelocity(wData.SuspensionPoint), up);
                newSusp = (Stiffness) * wData.CompressionRatio * up;
                deltaSusp = newSusp - currentSusp;
                rigidcar.AddForceAtPosition(deltaSusp, wData.SuspensionPoint, mode);
            }
            return wData.CompressionRatio;
        }

        private void UpdateSuspensionSystem()
        {
            /*
         * the normal of the surface calculated is calculated from the 4 suspension raycasts 
         * (those of them that hit a surface anyway) 
         */
            NumberOfRayHits = 0;
            Vector3 lastSurfaceNormal = AvgSurfaceNormal;
            for (int i = 0; i < wheelData.Length; i++)
            {
                ComputeSuspension(wheelData[i], ForceMode.Acceleration);
                if (wheelData[i].HitCheck)
                {
                    NumberOfRayHits++;
                    AvgSurfaceNormal += wheelData[i].HitAttr.normal;
                }
            }
            if (NumberOfRayHits > 0)
            {
                AvgSurfaceNormal /= NumberOfRayHits;
                OffTheGround = false;
            }
            else
            {
                OffTheGround = true;
                AvgSurfaceNormal = lastSurfaceNormal;
            }
        }

        private void ModelSuspension(SuspensionData wheelSuspension, GameObject wheelModel, float wheelRadius, Vector3 wheelDampVelocity)
        {
            Vector3 target = wheelSuspension.HitAttr.point + new Vector3 (0.0f, wheelRadius, 0.0f);
            wheelModel.transform.position = Vector3.SmoothDamp(wheelModel.transform.position, target, ref wheelDampVelocity, 0.05f); //, Mathf.Infinity, Time.fixedDeltaTime);
        }

        private void ResetFlippedCar()
        {
            float flipImpulse = Input.GetAxis("Jump");
            //print("Vector3.Angle es: " + Vector3.Angle(rigidcar.transform.up, avgSurfaceNormal));
            if (Vector3.Angle(myTransform.up, AvgSurfaceNormal) >= 80.0f) // Vector3.Angle returns unsigned angle
            {
                if (flipImpulse > 0.0f)
                {
                    rigidcar.rotation = Quaternion.Euler(0.0f, rigidcar.rotation.eulerAngles.y, 0.0f);
                }
            }
        }

        private void DrawDebugRays()
        {
            for (int i = 0; i < wheelData.Length; i++)
            {
                if (wheelData[i].HitCheck)
                {
                    Debug.DrawRay(wheelData[i].SuspensionPoint, myTransform.up * -wheelData[i].HitAttr.distance, new Color32(0, 255, 0, 255), 0.0f, false);
                }
                else
                {
                    Debug.DrawRay(wheelData[i].SuspensionPoint, -myTransform.up * SuspensionLength, new Color32(255, 128, 16, 255), 0.0f, false);
                }
            }
            Debug.DrawRay(myTransform.TransformPoint(rigidcar.centerOfMass), -myTransform.up * SuspensionLength, Color.red); //centro de masa
        }

        private void DrawDebugLabels()
        {
            if (Camera.main == null) return;
            for (int i = 0; i < wheelData.Length; i++)
            {
                Vector3 screenPosition;
                if (wheelData[i].HitCheck)
                {
                    screenPosition = Camera.main.WorldToScreenPoint(wheelData[i].HitAttr.point);
                }
                else
                {
                    screenPosition = Camera.main.WorldToScreenPoint(wheelData[i].SuspensionPoint - (myTransform.up * SuspensionLength));
                }

                //GUI.Label(new Rect(screenPosition.x, Screen.height - screenPosition.y, 25f, 20f), cr.ToString());
                Rect rect = new Rect(screenPosition.x, Screen.height - screenPosition.y, 25f, 20f);
                GUI.Label(rect, (wheelData[i].CompressionRatio).ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
        }
    }
}