using UnityEngine;
using UnityEngine.UI;

namespace Driving.Legacy
{
    [RequireComponent(typeof(FourWheelSuspension))]
    public class CarControllerLegacy : MonoBehaviour
    {
        private Rigidbody rigidcar;
        private BoxCollider carcollider;
        private FourWheelSuspension fws;

        public bool DebugDriving = false;

        // Player Input:
        private float powerInput;
        private float turnInput;

        // Speed
        [Range(1, 150)] public float MaxVelocitykmph = 120.0f; // in km/h
        private float MaxVelocity;
        private float InverseMaxVelocity;
        private float velocityMagnitude;
        private Vector3 localVelocity;
        //// Accel Forces
        public float Accel = 8.0f;
        public float TurnAccel = 3.0f;
        public float ReverseAccelMultiplier = 1.1f;

        //// Speedometer text
        [SerializeField] private Text velocityText = null;
        private readonly string velocityUnitText = " km/h";

        // Grip
        [Range(0, 1)] public float gripRatio = 1.0f;
        private Vector3 centerOfForces;
        [Range(0.6f, 0.80f)] public float CenterOfForcesRange = 0.65f; //0.65-0.75f values recommended for easier driving
        // Awake is called when the script instance is being loaded
        private void Awake()
        {
            rigidcar = GetComponent<Rigidbody>();
            carcollider = GetComponent<BoxCollider>();
            fws = GetComponent<FourWheelSuspension>();
            SetCenterOfForces();
        }

        // Start is called before the first frame update
        void Start()
        {
            MaxVelocity = MaxVelocitykmph / 3.6f; // convert to m/s
            InverseMaxVelocity = 1.0f / MaxVelocity; // calculate the inverse of the max velocity

            print("1.- centro de masa: " + rigidcar.centerOfMass);
            print("2.- Controller: centro de fuerzas: " + centerOfForces);
        }

        // Update is called once per frame
        void Update()
        {
#if (UNITY_EDITOR)
            if (DebugDriving)
            {
                Debug.DrawRay(transform.TransformPoint(centerOfForces), -transform.up * fws.SuspensionLength, Color.magenta);
            }
#endif
            if (velocityText != null)
            {
                velocityText.text = (velocityMagnitude * 3.6f).ToString("F0") + velocityUnitText;
            }
        }

        // This function is called every fixed framerate frame, if the MonoBehaviour is enabled
        private void FixedUpdate()
        {
            if (fws.OffTheGround)
            {
                return;
            }
            // Get player input
            powerInput = Input.GetAxis("Vertical");
            turnInput = Input.GetAxis("Horizontal");

            // Get local velocity vector
            localVelocity = transform.InverseTransformVector(rigidcar.velocity);
            float zVel = localVelocity.z;
            int zVelSign = VelocitySign(zVel);

            if (zVel <= 0.07f && zVel >= -0.07f)
            {
                EnsureStop();
            }
            // Stearing input easing depending on velocity
            float easing = EaseSteering(zVel, zVelSign);

            ApplyForces(zVelSign, easing);
            CapSpeed();
        }

        int VelocitySign(float zVel)
        {
            if (zVel > 0.0f || Mathf.Approximately(0.0f, zVel))
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        float EaseSteering(float zVel, int zVelSign)
        {
            //t = ratio between actual velocity and MaxVelicity
            float t = Mathf.Abs(zVel) * InverseMaxVelocity;  // t should always be between [0.0-1.0] for the lerp to work out
            float steeringCurve = Mathf.Lerp(0.0f, 1.0f, 2.0f * t * (2.0f - t));
            //print("vel sign: " + zVelSign + " ; zVel: " + zVel + "t: " + t + " -->> Input Turn Curve : " + steeringCurve);
            return steeringCurve;
        }

        void ApplyForces(int zVelSign, float easing)
        {

            Vector3 forwardForce = new Vector3(0.0f, 0.0f, (powerInput) * Accel);
            if (powerInput < 0.0f)
            {
                if (zVelSign > 0.0f)
                {
                    forwardForce = new Vector3(0.0f, 0.0f, powerInput * Accel * ReverseAccelMultiplier);
                }
                else
                {
                    forwardForce = new Vector3(0.0f, 0.0f, powerInput * Accel * (ReverseAccelMultiplier * 0.45f));
                }
            }
            Vector3 turnForce = new Vector3(turnInput * TurnAccel * easing * zVelSign, 0.0f, 0.0f);
            Vector3 projected = forwardForce - Vector3.Project(forwardForce, fws.avgSurfaceNormal.normalized);

            rigidcar.AddForceAtPosition(transform.TransformDirection(projected), transform.TransformPoint(centerOfForces));
            rigidcar.AddForceAtPosition(transform.TransformDirection(turnForce), transform.TransformPoint(centerOfForces), ForceMode.Force);
            ImproveTurnGrip(gripRatio);
        }

        void AdjustDrag(float velRatio)
        {
            //linear (0.05, 0.25)
            rigidcar.drag = Mathf.Lerp(0.25f, 0.15f, velRatio);

            //angular (0.25, 1)
            //rigidcar.angularDrag = Mathf.Lerp(1.5f, 0.90f, velRatio);
        }

        void ImproveTurnGrip(float ratio)
        {
            // get lateral component in local space
            Vector3 velocityComponent = new Vector3(transform.InverseTransformDirection(rigidcar.velocity).x, 0.0f, 0.0f);
            rigidcar.AddRelativeForce(-velocityComponent * (ratio + 1.05f), ForceMode.Acceleration);
            return;
        }

        // This function caps velocity when no input is given and when max velocity reached
        void CapSpeed()
        {
            //velocityMagnitude = rigidcar.velocity.magnitude;
            velocityMagnitude = Vector3.Dot(rigidcar.velocity, transform.forward);
            if (velocityMagnitude > (MaxVelocity))
            {
                rigidcar.velocity = rigidcar.velocity.normalized * MaxVelocity;
            }
        }

        // Ensures the car stop in the local XZ plane
        void EnsureStop()
        {
            // Because of suspension depends on wheel positions, the car might be slightly tilted.
            // For low velocities, we counter the force from gravity components with this 2 forces:
            Vector3 xStop = -Vector3.Dot(Physics.gravity, transform.right) * transform.right;
            Vector3 zStop = -Vector3.Dot(Physics.gravity, transform.forward) * transform.forward;
            rigidcar.AddForce(zStop, ForceMode.Force);
            rigidcar.AddForce(xStop, ForceMode.Force);
        }

        void SetCenterOfForces()
        {
            Quaternion storedRotation = rigidcar.rotation;
            rigidcar.rotation = Quaternion.identity;

            centerOfForces = Vector3.Scale(carcollider.bounds.extents * CenterOfForcesRange, new Vector3(0.0f, -1.0f, 1.0f));
            centerOfForces = rigidcar.centerOfMass + centerOfForces;
            rigidcar.rotation = storedRotation;
        }
    }
}
