﻿using UnityEngine;

namespace RoadManagement
{
    [RequireComponent(typeof(BoxCollider))]
    public class MiddleCollider : MonoBehaviour
    {
        private TrafficLightController parent;
        private BoxCollider myCollider;
    
        // Start is called before the first frame update
        void Start()
        {
            myCollider = GetComponent<BoxCollider>();
            parent = GetComponentInParent<TrafficLightController>();
        }
    
        private void OnTriggerEnter(Collider car)
        {
            parent.ReceiveMiddleTrigger(myCollider, car);
        }
    }
}
