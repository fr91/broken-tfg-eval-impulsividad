﻿using System;
using System.Collections;
using System.Collections.Generic;
using Driving;
using UnityEngine;

public class TrackDrivingEnd : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        CarUserController user = other.GetComponent<CarUserController>();
        user.DrivingTrialEnd();
    }
}
