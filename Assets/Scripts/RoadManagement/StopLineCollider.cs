﻿using System;
using UnityEngine;

namespace RoadManagement
{
    [RequireComponent(typeof(BoxCollider))]
    public class StopLineCollider : MonoBehaviour
    {
        private TrafficLightController parent;
        private BoxCollider myCollider;
        
        // Start is called before the first frame update
        void Start()
        {
            myCollider = GetComponent<BoxCollider>();
            parent = GetComponentInParent<TrafficLightController>();
        }

        private void OnTriggerEnter(Collider car)
        {
            parent.ReceiveStopLineTriggerEnter(myCollider, car);
        }

        private void OnTriggerExit(Collider car)
        {
            parent.ReceiveStopLineTriggerExit(myCollider, car);
        }
    }
}
