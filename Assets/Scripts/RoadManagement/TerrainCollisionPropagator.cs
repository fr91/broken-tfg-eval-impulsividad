﻿using System;
using UnityEngine;

namespace RoadManagement
{
    public class TerrainCollisionPropagator : MonoBehaviour
    {
        private Terrain terrain; // the terrain that has attached this script

        private void Awake()
        {
            terrain = GetComponentInParent<Terrain>();
        }

        private void OnTriggerEnter(Collider other)
        {
            EndlessRoad.UsherTerrain(terrain);
            //Invoke(nameof(DeferredPropagation), time: 0.1f);
        }

        private void DeferredPropagation()
        {
            EndlessRoad.UsherTerrain(terrain);
        }
        
    }
}