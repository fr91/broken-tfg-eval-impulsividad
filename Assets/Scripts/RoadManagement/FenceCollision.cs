﻿using Measuring;
using UnityEngine;

namespace RoadManagement
{
    public class FenceCollision : MonoBehaviour
    {
    
        private void OnCollisionEnter(Collision other)
        {
            GameObject car = other.gameObject;
            RoadCenterCalculator centerCalc = car.GetComponentInChildren<RoadCenterCalculator>();
            centerCalc.ResetToRoadCenter();
        }

        
    }
}
