﻿using System;
using UnityEngine;

namespace RoadManagement
{
    public class BackTerrainDestroyer : MonoBehaviour
    {
        
        private Terrain terrain; // the terrain that has attached this script
        private GameObject parent;
        private void Start()
        {
            terrain = GetComponentInParent<Terrain>();
            parent = transform.parent.gameObject;
        }


        private void OnTriggerEnter(Collider other)
        {
            Destroy(parent);
        }
        
    }
}