# TFG-Impulsividad

To modify the project after cloning it:

0. Add the contents `copyme-unity-git-config` to .git/ with the name `config`:
```shell
$ cat copyme-unity-git-config >> .git/config
```
1. (Recommended) Install plugin GitHub-for-Unity
2. (Optional) Re-Install road plugin
3. Run the Unity Project
4. Configure the script editor in the preferences for you (cloning this project might be set up with JetBrains Rider)